class SmartTV
{
    get TVMake()
    {
        return this.SmartTVMake;
    }
    set TVMake(value)
    {
        this.SmartTVMake = value;
    }
    get TVModel()
    {
        return this.SmartTVModel;
    }
    set TVModel(value)
    {
        this.SmartTVModel = value;
    }
    get Size()
    {
        return this.SmartTVSize;
    }
    set Size(value)
    {
        this.SmartTVSize = value;
    }
    get Price()
    {
        return this.SmartTVPrice;
    }
    set Price(value)
    {
        this.SmartTVPrice = value;
    }
    ccPrice(levy)
    {
        levy = levy / 100;
        return `Price using Credit Card: $${this.SmartTVPrice + (this.SmartTVPrice * levy)}`;
    }
    constructor(TVMake, TVModel, Size, Price)
    {
        this.SmartTVMake = TVMake;
        this.SmartTVModel = TVModel;
        this.SmartTVSize = Size;
        this.SmartTVPrice = Price;
    }
}

var TV1 = new SmartTV();
TV1.TVMake = prompt("Welcome to SmartTVs Limited Stock Organiser. Enter the Make of the SmartTV: ");
TV1.TVModel = prompt("Enter the Model of the SmartTV: ");
TV1.Size = prompt("Enter the screen size (inches): ");
TV1.Price = Number(prompt("Enter the price of the SmartTV: "));
console.log(`--- SmartTV information ---\n\nSmartTV Make: ${TV1.TVMake}\nSmartTV Model: ${TV1.TVModel}\nSmartTV Size: ${TV1.Size} inches\nSmartTV Price: $${TV1.Price}`);
var levy = Number(prompt("Enter the percentage levy you would like to apply to this SmartTV (%): "));
console.log(TV1.ccPrice(levy));